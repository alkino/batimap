import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-unsubscriber',
    template: '',
})
//tslint:disable
export class Unsubscriber implements OnDestroy {
    private readonly subscriptions: Subscription[] = [];

    autoUnsubscribe(subscription: Subscription) {
        this.subscriptions.push(subscription);
    }
    ngOnDestroy() {
        this.subscriptions.forEach(it => it.unsubscribe());
    }
}
